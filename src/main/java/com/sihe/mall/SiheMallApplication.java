package com.sihe.mall;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@MapperScan(basePackages = "com.sihe.mall.model.dao")
@EnableSwagger2
public class SiheMallApplication {
    public static void main(String[] args) {
        SpringApplication.run(SiheMallApplication.class, args);
    }

}
