package com.sihe.mall.service;


import com.sihe.mall.exception.siheMallException;
import com.sihe.mall.model.vo.CartVO;

import java.util.List;

/**
 * 描述：     购物车Service
 */
public interface CartService {

    List<CartVO> list(Integer userId);

    List<CartVO> add(Integer userId, Integer productId, Integer count) throws siheMallException;

    List<CartVO> update(Integer userId, Integer productId, Integer count) throws siheMallException;

    List<CartVO> delete(Integer userId, Integer productId) throws siheMallException;

    List<CartVO> selectOrNot(Integer userId, Integer productId, Integer selected) throws siheMallException;

    List<CartVO> selectAllOrNot(Integer userId, Integer selected);
}
