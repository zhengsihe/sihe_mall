package com.sihe.mall.service;


import com.sihe.mall.exception.siheMallException;
import com.sihe.mall.model.pojo.User;

/**
 * 描述：     UserService
 */

public interface UserService {

    User getUser();

    void register(String userName, String password) throws siheMallException;

    User login(String userName, String password) throws siheMallException;

    void updateInformation(User user) throws siheMallException;

    boolean checkAdminRole(User user);
}
