package com.sihe.mall.service;

import com.github.pagehelper.PageInfo;
import com.sihe.mall.exception.siheMallException;
import com.sihe.mall.model.pojo.Product;
import com.sihe.mall.model.request.AddProductReq;
import com.sihe.mall.model.request.ProductListReq;

/**
 * 描述：     商品Service
 */
public interface ProductService {

    void add(AddProductReq addProductReq) throws siheMallException;

    void update(Product updateProduct) throws siheMallException;

    void delete(Integer id) throws siheMallException;

    void batchUpdateSellStatus(Integer[] ids, Integer sellStatus);

    PageInfo listForAdmin(Integer pageNum, Integer pageSize);

    Product detail(Integer id);

    PageInfo list(ProductListReq productListReq);
}
