package com.sihe.mall.service;

import com.github.pagehelper.PageInfo;
import com.sihe.mall.exception.siheMallException;
import com.sihe.mall.model.request.CreateOrderReq;
import com.sihe.mall.model.vo.OrderVO;

/**
 * 描述：     订单Service
 */

public interface OrderService {


    String create(CreateOrderReq createOrderReq) throws siheMallException;

    OrderVO detail(String orderNo) throws siheMallException;

    PageInfo listForCustomer(Integer pageNum, Integer pageSize) throws siheMallException;

    void cancel(String orderNo) throws siheMallException;

    String qrcode(String orderNo);

    void pay(String orderNo) throws siheMallException;

    PageInfo listForAdmin(Integer pageNum, Integer pageSize) throws siheMallException;

    void deliver(String orderNo) throws siheMallException;

    void finish(String orderNo) throws siheMallException;
}
