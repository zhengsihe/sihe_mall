package com.sihe.mall.service;

import com.github.pagehelper.PageInfo;
import com.sihe.mall.exception.siheMallException;
import com.sihe.mall.model.pojo.Category;
import com.sihe.mall.model.request.AddCategoryReq;
import com.sihe.mall.model.vo.CategoryVO;

import java.util.List;


/**
 * 描述：     分类目录Service
 */
public interface CategoryService {

    void add(AddCategoryReq addCategoryReq) throws siheMallException;

    void update(Category updateCategory) throws siheMallException;

    void delete(Integer id) throws siheMallException;

    PageInfo listForAdmin(Integer pageNum, Integer pageSize);

//    List<CategoryVO> listCategoryForCustomer();

    List<CategoryVO> listCategoryForCustomer(Integer parentId);
}
