package com.sihe.mall.exception;

import com.sihe.mall.common.ApiRestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 描述：     处理统一异常的handler
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    //日志打印
    private final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseBody
    //系统异常
    public Object handleException(Exception e) {
        log.error("Default Exception: ", e);
        return ApiRestResponse.error(siheMallExceptionEnum.SYSTEM_ERROR);
    }

    @ExceptionHandler(siheMallException.class)
    @ResponseBody
    //业务异常
    public Object handleImoocMallException(siheMallException e) {
        log.error("siheMallException: ", e);
        return ApiRestResponse.error(e.getCode(), e.getMessage());
    }
}
