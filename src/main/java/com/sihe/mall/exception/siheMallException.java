package com.sihe.mall.exception;

/**
 * 描述：     统一异常的格式
 */
public class siheMallException extends  Exception {
    private final Integer code;
    private final String message;

    public siheMallException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public siheMallException(siheMallExceptionEnum exceptionEnum) {
        this(exceptionEnum.getCode(), exceptionEnum.getMsg());
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}

