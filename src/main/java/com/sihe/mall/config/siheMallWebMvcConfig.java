package com.sihe.mall.config;


import com.sihe.mall.common.Constant;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 描述：     配置地址映射
 */
@Configuration

public class siheMallWebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/admin/**").addResourceLocations("classpath:/static/admin/");
        registry.addResourceHandler("/images/**").addResourceLocations("file:" + Constant.FILE_UPLOAD_DIR);
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

//    @Value("${cors.allowedOrigins}")
//    private String allowedOrigins;
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedHeaders("Content-Type", "X-Request-With", "Access-Control-Request-Method", "Access-Control-Request-Headers", "token")
//                .allowedOrigins("http://localhost:8083", "https://www.example.com") // 允许本地路径进行跨域请求
//                .allowedMethods("GET", "POST", "PUT", "DELETE") // 允许的HTTP方法
//                .allowedMethods("*")
                .allowCredentials(true);
//                .allowedHeaders("Content-Type", "Authorization") // 允许的请求头
//                .maxAge(3600); // 预检请求的最大缓存时间


    }

}
